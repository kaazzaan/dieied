package com.kaazzaan.deathparade.utils;

import android.content.Context;
import android.util.Xml;

import com.kaazzaan.deathparade.R;
import com.kaazzaan.deathparade.models.Answer;
import com.kaazzaan.deathparade.models.Question;
import com.kaazzaan.deathparade.models.QuestionType;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by daniyarsafiullin on 10/03/2017.
 */

public class QuestionsParser {
    private final Context context;

    public QuestionsParser(Context context) {
        this.context = context;
    }

    public List<Question> parse() {
        List<Question> result = new ArrayList<>();

        InputStream in = context.getResources().openRawResource(R.raw.questions);

        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            result.addAll(readQuestions(parser));
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    private List<Question> readQuestions(XmlPullParser parser) throws XmlPullParserException, IOException {

        List<Question> result = new ArrayList<>();
        parser.require(XmlPullParser.START_TAG, null, "questions");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            // Starts by looking for the entry tag
            if (name.equals("question")) {
                result.add(readQuestion(parser));
            }
        }

        return result;
    }

    private Question readQuestion(XmlPullParser parser) throws XmlPullParserException, IOException {
        Question item = new Question();

        parser.require(XmlPullParser.START_TAG, null, "question");

        String right = parser.getAttributeValue(null, "id");
        item.id = Integer.parseInt(right);
        String type = parser.getAttributeValue(null, "type");
        item.type = QuestionType.valueOf(type);

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("title")) {
                item.title = readString(parser, "title");
            } else if (name.equals("answer")) {
                item.answers.add(readAnswer(parser));
            }
        }

        parser.require(XmlPullParser.END_TAG, null, "answers");
        parser.nextTag();
        parser.require(XmlPullParser.END_TAG, null, "question");
        return item;
    }

    private Answer readAnswer(XmlPullParser parser) throws XmlPullParserException, IOException {
        Answer item = new Answer();

        parser.require(XmlPullParser.START_TAG, null, "answer");
//            if (parser.getEventType() != XmlPullParser.START_TAG) {
//                continue;
//            }

//            String tag = parser.getName();
        String right = parser.getAttributeValue(null, "id");
        item.id = Integer.parseInt(right);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("title")) {
                item.title = readString(parser, "title");
            } else if (name.equals("multiple_factor")) {
                item.multipleFactor = Float.parseFloat(readText(parser));
            }
        }

        parser.require(XmlPullParser.END_TAG, null, "answer");

        return item;
    }

    protected String readString(XmlPullParser parser, String tagName) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, tagName);
        String text = readText(parser);
        parser.require(XmlPullParser.END_TAG, null, tagName);
        return text;
    }


    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }
}
