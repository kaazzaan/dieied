package com.kaazzaan.deathparade.utils;

import android.content.Context;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Created  on 08/08/16.
 */
class BaseXmlParser {
    protected final Context context;

    public BaseXmlParser(Context context) {
        this.context = context;
    }

    protected String readString(XmlPullParser parser, String tagName) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, tagName);
        String text = readText(parser);
        parser.require(XmlPullParser.END_TAG, null, tagName);
        return text;
    }

    protected String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText().trim();
            parser.nextTag();
        }
        return result;
    }
}
