package com.kaazzaan.deathparade.utils;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by daniyarsafiullin on 11/03/2017.
 */

public class Utils {
    public static final String PREF_AGE = "prefs_age";
    public static final String PREF_TOTAL_TIME = "prefs_total_time";
    public static final String PREF_TEST_DATE = "prefs_test_date";
    public static final String PREF_CURRENT_AGE = "prefs_current_age";
    public static final String PREF_GENDER_MALE = "prefs_gender_male";
    public static final String PREF_COUNTRY = "prefs_country";
    public static final String PREF_MULTPLE_FACTOR = "prefs_factor";
    public static final String PREF_COUNTRY_POSITION = "prefs_country_position";

    public static void hideKeyboard(Context context, View view) {
        InputMethodManager inputManager = (InputMethodManager) context.
                getSystemService(Context.INPUT_METHOD_SERVICE);
        if (view != null) {
            inputManager.hideSoftInputFromWindow(view.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
