package com.kaazzaan.deathparade.utils;

import android.content.Context;
import android.util.Xml;

import com.kaazzaan.deathparade.R;
import com.kaazzaan.deathparade.models.Country;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by daniyarsafiullin on 11/03/2017.
 */

public class CountriesParser extends BaseXmlParser {
    public CountriesParser(Context context) {
        super(context);
    }

    public ArrayList<Country> parse() {
        ArrayList<Country> result = new ArrayList<>();

        InputStream in = context.getResources().openRawResource(R.raw.countries);

        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            result.addAll(readCountries(parser));
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    private ArrayList<Country> readCountries(XmlPullParser parser) throws XmlPullParserException, IOException {
        ArrayList<Country> countries = new ArrayList<>();

        parser.require(XmlPullParser.START_TAG, null, "countries");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            // Starts by looking for the entry tag
            if (name.equals("country")) {
                countries.add(readCountry(parser));
            }
        }

        return countries;
    }

    private Country readCountry(XmlPullParser parser) throws XmlPullParserException, IOException {
        Country country = new Country();

        parser.require(XmlPullParser.START_TAG, null, "country");


        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("name")) {
                country.name = readString(parser, "name");
            } else if (name.equals("male")) {
                String lfe = readString(parser, "male");
                country.menLifeExpectancy = Float.parseFloat(lfe);
            } else if (name.equals("female")) {
                String lfe = readString(parser, "female");
                country.womenLifeExpectancy = Float.parseFloat(lfe);
            }
        }

        parser.require(XmlPullParser.END_TAG, null, "life_expectancy");
        parser.nextTag();
        parser.require(XmlPullParser.END_TAG, null, "country");

        return country;
    }
}
