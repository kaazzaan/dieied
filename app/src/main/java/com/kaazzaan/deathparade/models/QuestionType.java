package com.kaazzaan.deathparade.models;

/**
 * Created by daniyarsafiullin on 10/03/2017.
 */

public enum QuestionType {
    selector, input, country
}
