package com.kaazzaan.deathparade.models;

import java.util.ArrayList;

/**
 * Created by daniyarsafiullin on 10/03/2017.
 */

public class Question {
    public int id;
    public ArrayList<Answer> answers = new ArrayList<>();
    public QuestionType type;
    public String title;
}
