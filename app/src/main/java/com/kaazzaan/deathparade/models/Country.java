package com.kaazzaan.deathparade.models;

import java.io.Serializable;

public class Country implements Serializable {
    public String name;
    public float menLifeExpectancy;
    public float womenLifeExpectancy;
}
