package com.kaazzaan.deathparade.ui.fragments;

import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kaazzaan.deathparade.DieApplication;
import com.kaazzaan.deathparade.R;
import com.kaazzaan.deathparade.ui.adapters.TestQuestionsAdapter;
import com.kaazzaan.deathparade.utils.Utils;

import java.util.Date;

import butterknife.BindView;

/**
 * Created by daniyarsafiullin on 11/03/2017.
 */

public class TestFragment extends BaseFragment {
    @BindView(R.id.questions_count)
    protected TextView counter;
    @BindView(R.id.questions_list)
    protected RecyclerView list;

    private TestQuestionsAdapter adapter;
    private String counterTemplate;
    BroadcastReceiver scrollPageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int pageId = intent.getIntExtra("page_id", -1);
            if (pageId == adapter.getItemCount()) {
                showResult();
            }
            if (list != null && pageId >= 0) {
                list.scrollToPosition(pageId);
                counter.setText(String.format(counterTemplate, pageId + 1, adapter.getItemCount()));
            }
        }
    };

    private void showResult() {
        long totalLife = (long) (DieApplication.calculatedLifeExpectancy * DateUtils.YEAR_IN_MILLIS
                + DateUtils.DAY_IN_MILLIS * Math.random() * 25
                + DateUtils.HOUR_IN_MILLIS * 23
                + DateUtils.MINUTE_IN_MILLIS * 59
                + DateUtils.SECOND_IN_MILLIS * 60);
        PreferenceManager.getDefaultSharedPreferences(getActivity()).edit()
                .putFloat(Utils.PREF_AGE, DieApplication.calculatedLifeExpectancy)
                .putLong(Utils.PREF_TOTAL_TIME, totalLife)
                .putLong(Utils.PREF_TEST_DATE, new Date().getTime())
                .putFloat(Utils.PREF_MULTPLE_FACTOR, DieApplication.multipleFactor)
                .apply();
        FragmentTransaction ft = getActivity().getFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_container, new ResultFragment());
        ft.commitAllowingStateLoss();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_test, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new TestQuestionsAdapter(DieApplication.questions);
        LinearLayoutManager manager = new LinearLayoutManager(getActivity()) {
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }
        };
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        list.setLayoutManager(manager);
        list.setAdapter(adapter);

        counterTemplate = getString(R.string.question_counter);
        counter.setText(String.format(counterTemplate, 1, adapter.getItemCount()));
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(scrollPageReceiver, new IntentFilter("scroll_test_list"));
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(scrollPageReceiver);
    }
}
