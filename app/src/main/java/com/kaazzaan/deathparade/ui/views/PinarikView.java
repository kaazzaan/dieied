package com.kaazzaan.deathparade.ui.views;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.kaazzaan.deathparade.R;

/**
 * Created by daniyarsafiullin on 12/03/2017.
 */

public class PinarikView extends LinearLayout {
    private float age = 32f;
    private float lifeExpectancy = 68.5f;

    public PinarikView(Context context) {
        super(context);
        init();
    }

    public PinarikView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PinarikView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public PinarikView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public void setData(float currentAge, float lifeExpectancy) {
        this.age = currentAge;
        this.lifeExpectancy = lifeExpectancy;
    }

    private void init() {
        setOrientation(LinearLayout.VERTICAL);
        addMonths();
    }

    private void addMonths() {
        int rows = (int) Math.ceil(lifeExpectancy);
        if (lifeExpectancy < age) {
            rows = (int) Math.ceil(age);
        }
        int lastYearMonthsCount = (int) Math.ceil((rows - lifeExpectancy) * 12);
        int totalLifeExpectancyMonths = (int) Math.ceil(lifeExpectancy * 12);
        int totalMyMonths = (int) Math.ceil(age * 12);
        int monthCount = 1;
        for (int year = 0; year < rows; year++) {
            LinearLayout yearContainer = new LinearLayout(getContext());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.weight = 1;
            yearContainer.setLayoutParams(params);
            addView(yearContainer);

            for (int month = 0; month < 12; month++) {
                View item = new View(getContext());
                LinearLayout.LayoutParams itemParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
                itemParams.weight = 1;
                itemParams.topMargin = 5;
                itemParams.bottomMargin = 5;
                itemParams.leftMargin = 5;
                itemParams.rightMargin = 5;
                item.setLayoutParams(itemParams);
                yearContainer.addView(item);
                if (monthCount < totalMyMonths) {
                    if (lifeExpectancy < age && totalLifeExpectancyMonths < monthCount) {

                        item.setBackgroundColor(Color.GREEN);
                    } else {
                        item.setBackgroundColor(Color.BLACK);
                    }
                } else {
                    item.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                }

                monthCount++;
            }
        }
    }

    public void redrawMonths() {
        removeAllViews();
        addMonths();
    }
}
