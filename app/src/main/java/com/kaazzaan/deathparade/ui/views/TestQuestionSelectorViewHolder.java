package com.kaazzaan.deathparade.ui.views;

import android.content.Context;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.kaazzaan.deathparade.DieApplication;
import com.kaazzaan.deathparade.R;
import com.kaazzaan.deathparade.models.Answer;
import com.kaazzaan.deathparade.models.Question;
import com.kaazzaan.deathparade.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.OnItemClick;

/**
 * Created by daniyarsafiullin on 11/03/2017.
 */

public class TestQuestionSelectorViewHolder extends TestQuestionViewHolder {

    @BindView(R.id.question_answers)
    protected ListView answersContainer;
    private AnswersListAdapter adapter;

    public TestQuestionSelectorViewHolder(View view) {
        super(view);

    }

    @Override
    public void bindView(Question question) {
        super.bindView(question);
        adapter = new AnswersListAdapter(itemView.getContext(), question.answers);
        answersContainer.setAdapter(adapter);
    }

    @OnItemClick(R.id.question_answers)
    protected void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        answer = question.answers.get(position);
        if (question.id == 1) {
            DieApplication.male = answer.multipleFactor == 1;
            PreferenceManager.getDefaultSharedPreferences(itemView.getContext()).edit().putBoolean(Utils.PREF_GENDER_MALE, DieApplication.male).apply();
        }
        calculateLE();
    }

    protected class AnswersListAdapter extends ArrayAdapter<Answer> {

        public AnswersListAdapter(@NonNull Context context, @NonNull List<Answer> objects) {
            super(context, R.layout.view_test_question, R.id.question, objects);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            View view = super.getView(position, convertView, parent);
            ((TextView) view.findViewById(R.id.question)).setText(getItem(position).title);
            return view;
        }
    }

}
