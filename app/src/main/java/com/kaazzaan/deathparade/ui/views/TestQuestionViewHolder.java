package com.kaazzaan.deathparade.ui.views;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.kaazzaan.deathparade.DieApplication;
import com.kaazzaan.deathparade.R;
import com.kaazzaan.deathparade.models.Answer;
import com.kaazzaan.deathparade.models.Question;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by daniyarsafiullin on 11/03/2017.
 */

public class TestQuestionViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.question)
    protected TextView questionTitle;

    protected Question question;
    protected Answer answer;

    public TestQuestionViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bindView(Question question) {
        this.question = question;

        questionTitle.setText(question.title);
    }

    public void calculateLE() {
        if (answer != null) {
            DieApplication.multipleFactor *= answer.multipleFactor;
            DieApplication.calculatedLifeExpectancy *= answer.multipleFactor;
        }

        Intent nextPageIntent = new Intent("scroll_test_list");
        nextPageIntent.putExtra("page_id", question.id);
        itemView.getContext().sendBroadcast(nextPageIntent);
    }
}
