package com.kaazzaan.deathparade.ui.views;

import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.EditText;

import com.kaazzaan.deathparade.DieApplication;
import com.kaazzaan.deathparade.R;
import com.kaazzaan.deathparade.models.Question;
import com.kaazzaan.deathparade.utils.Utils;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by daniyarsafiullin on 11/03/2017.
 */

public class TestQuestionInputViewHolder extends TestQuestionViewHolder {
    @BindView(R.id.question_answer)
    protected EditText answer;

    public TestQuestionInputViewHolder(View view) {
        super(view);
    }

    @Override
    public void bindView(Question question) {
        super.bindView(question);
    }


    @OnClick(R.id.question_next_question)
    protected void onNextClick() {
        Utils.hideKeyboard(itemView.getContext(), answer);
        if (answer.getText().length() == 0) {
            answer.setError(itemView.getResources().getString(R.string.empty_field_error));
            Snackbar.make(itemView, R.string.empty_field_error, Snackbar.LENGTH_SHORT).show();
        } else {
            DieApplication.calculatedLifeExpectancy = Float.parseFloat(answer.getText().toString());
            if (DieApplication.calculatedLifeExpectancy < 12) {
//                itemView.getContext().sendBroadcast(new Intent("so_young"));
                answer.setError(itemView.getResources().getString(R.string.so_young_error));
                Snackbar.make(itemView, R.string.so_young_error, Snackbar.LENGTH_SHORT).show();
            } else {
                PreferenceManager.getDefaultSharedPreferences(itemView.getContext()).edit().putFloat(Utils.PREF_CURRENT_AGE, DieApplication.calculatedLifeExpectancy).apply();
                super.calculateLE();
            }
        }
    }
}
