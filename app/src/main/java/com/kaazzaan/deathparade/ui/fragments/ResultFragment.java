package com.kaazzaan.deathparade.ui.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kaazzaan.deathparade.BuildConfig;
import com.kaazzaan.deathparade.DieApplication;
import com.kaazzaan.deathparade.R;
import com.kaazzaan.deathparade.ui.views.PinarikView;
import com.kaazzaan.deathparade.utils.Utils;

import butterknife.BindView;

/**
 * Created by daniyarsafiullin on 11/03/2017.
 */

public class ResultFragment extends BaseFragment {
    @BindView(R.id.title)
    protected TextView title;
    @BindView(R.id.title_ago)
    protected TextView titleAgo;
    @BindView(R.id.result_years)
    protected TextView yearsView;
    @BindView(R.id.result_months)
    protected TextView monthsView;
    @BindView(R.id.result_days)
    protected TextView daysView;
    @BindView(R.id.result_hours)
    protected TextView hoursView;
    @BindView(R.id.result_minutes)
    protected TextView minutesView;
    @BindView(R.id.result_seconds)
    protected TextView secondsView;
    @BindView(R.id.result_percentage)
    protected TextView percentage;
    @BindView(R.id.result_pinarik)
    protected PinarikView pinarik;
    @BindView(R.id.advertise)
    protected AdView adView;
    private float currentYearsPref;
    private long testDate;
    private long totalYearsMillis;
    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            updateValues();
            handler.postDelayed(runnable, DateUtils.SECOND_IN_MILLIS);
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_result, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // result.setText(String.format(getString(R.string.estimated_template), DieApplication.calculatedLifeExpectancy));
        currentYearsPref = PreferenceManager.getDefaultSharedPreferences(getActivity()).getFloat(Utils.PREF_CURRENT_AGE, 0);
        testDate = PreferenceManager.getDefaultSharedPreferences(getActivity()).getLong(Utils.PREF_TEST_DATE, 0);
        totalYearsMillis = PreferenceManager.getDefaultSharedPreferences(getActivity()).getLong(Utils.PREF_TOTAL_TIME, 0);
        float dx = ((float) (System.currentTimeMillis() - testDate)) / DateUtils.YEAR_IN_MILLIS;
        float currentYears = currentYearsPref + dx;
        float percentageVal = (currentYears / DieApplication.calculatedLifeExpectancy) * 100;
        percentage.setText(String.format(getString(R.string.lived_template), percentageVal));
        pinarik.setData(currentYears, DieApplication.calculatedLifeExpectancy);
        pinarik.redrawMonths();
        updateValues();
        if (currentYearsPref > DieApplication.calculatedLifeExpectancy) {
            title.setText(R.string.you_should_die_ago);
            titleAgo.setVisibility(View.VISIBLE);
        }
//App package, google app id: com.kaazzaan.deathparade, 1:175124439672:android:0a9655b9a6129117
        AdRequest.Builder adBuilder = new AdRequest.Builder();
        if (BuildConfig.DEBUG) {
            adBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
            adBuilder.addTestDevice("604AB5155E8D2BF9DF3C90923F1F050C");
        }
        AdRequest adRequest = adBuilder.build();
//        adView.setAdSize(AdSize.SMART_BANNER);
//        adView.loadAd(adRequest);

    }

    @Override
    public void onResume() {
        super.onResume();
        handler.postDelayed(runnable, DateUtils.SECOND_IN_MILLIS);
    }

    @Override
    public void onPause() {
        super.onPause();
        handler.removeCallbacks(runnable);
    }

    private void updateValues() {
        long dx = System.currentTimeMillis() - testDate;
        long currentYearsMillis = (long) (currentYearsPref * DateUtils.YEAR_IN_MILLIS) + dx;
        long timeToDie = totalYearsMillis - currentYearsMillis;


        int years = (int) Math.floor(timeToDie / DateUtils.YEAR_IN_MILLIS);
        long millis = timeToDie - years * DateUtils.YEAR_IN_MILLIS;
        long monthMillis = DateUtils.YEAR_IN_MILLIS / 12;
        int months = (int) Math.floor(millis / monthMillis);
        millis -= months * monthMillis;
        int days = (int) Math.floor(millis / DateUtils.DAY_IN_MILLIS);
        millis -= days * DateUtils.DAY_IN_MILLIS;
        int hours = (int) Math.floor(millis / DateUtils.HOUR_IN_MILLIS);
        millis -= hours * DateUtils.HOUR_IN_MILLIS;
        int minutes = (int) Math.floor(millis / DateUtils.MINUTE_IN_MILLIS);
        millis -= minutes * DateUtils.MINUTE_IN_MILLIS;
        int seconds = (int) Math.floor(millis / DateUtils.SECOND_IN_MILLIS);
        years = Math.abs(years);
        months = Math.abs(months);
        days = Math.abs(days);
        hours = Math.abs(hours);
        minutes = Math.abs(minutes);
        seconds = Math.abs(seconds);
        yearsView.setText("" + years);
        monthsView.setText("" + months);
        daysView.setText("" + days);
        hoursView.setText("" + hours);
        minutesView.setText("" + minutes);
        secondsView.setText("" + seconds);

//        int months = (int) Math.floor((timeToDie - years * DateUtils.YEAR_IN_MILLIS) / monthMillis);
//        int days = (int) Math.floor((timeToDie - years * DateUtils.YEAR_IN_MILLIS - months * monthMillis) / DateUtils.DAY_IN_MILLIS);
//        int hours = (int) Math.floor((timeToDie - years * DateUtils.YEAR_IN_MILLIS - months * monthMillis - days * DateUtils.DAY_IN_MILLIS) / DateUtils.HOUR_IN_MILLIS);
//        int minutes = (int) Math.floor((timeToDie - years * DateUtils.YEAR_IN_MILLIS
//                - months * monthMillis - days * DateUtils.DAY_IN_MILLIS - hours * DateUtils.HOUR_IN_MILLIS) / DateUtils.MINUTE_IN_MILLIS);
//        int seconds = (int) Math.floor((timeToDie - years * DateUtils.YEAR_IN_MILLIS
//                - months * monthMillis - days * DateUtils.DAY_IN_MILLIS - hours * DateUtils.HOUR_IN_MILLIS - minutes * DateUtils.MINUTE_IN_MILLIS) / DateUtils.SECOND_IN_MILLIS);
//        yearsView.setText("" + years);
//        monthsView.setText("" + months);
//        daysView.setText("" + days);
//        hoursView.setText("" + hours);
//        minutesView.setText("" + minutes);
//        secondsView.setText("" + seconds);
    }
}

