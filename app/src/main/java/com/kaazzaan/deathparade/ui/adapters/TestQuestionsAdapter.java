package com.kaazzaan.deathparade.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.kaazzaan.deathparade.R;
import com.kaazzaan.deathparade.models.Question;
import com.kaazzaan.deathparade.models.QuestionType;
import com.kaazzaan.deathparade.ui.views.TestQuestionCountryViewHolder;
import com.kaazzaan.deathparade.ui.views.TestQuestionInputViewHolder;
import com.kaazzaan.deathparade.ui.views.TestQuestionSelectorViewHolder;
import com.kaazzaan.deathparade.ui.views.TestQuestionViewHolder;

import java.util.List;

/**
 * Created by daniyarsafiullin on 11/03/2017.
 */

public class TestQuestionsAdapter extends RecyclerView.Adapter<TestQuestionViewHolder> {
    private final List<Question> questions;

    public TestQuestionsAdapter(List<Question> questions) {
        this.questions = questions;
    }

    @Override
    public TestQuestionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (QuestionType.values()[viewType]) {

            case selector:
                return new TestQuestionSelectorViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_test_question_selector, parent, false));
            case input:
                return new TestQuestionInputViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_test_question_input, parent, false));
            case country:
                return new TestQuestionCountryViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_test_question_selector, parent, false));
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        return questions.get(position).type.ordinal();
    }

    @Override
    public void onBindViewHolder(TestQuestionViewHolder holder, int position) {
        holder.bindView(questions.get(position));
    }

    @Override
    public int getItemCount() {
        return questions.size();
    }
}
