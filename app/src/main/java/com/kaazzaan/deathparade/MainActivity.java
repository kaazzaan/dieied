package com.kaazzaan.deathparade;

import android.app.Fragment;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.ads.MobileAds;
import com.kaazzaan.deathparade.ui.fragments.AboutFragment;
import com.kaazzaan.deathparade.ui.fragments.ResultFragment;
import com.kaazzaan.deathparade.ui.fragments.TestFragment;
import com.kaazzaan.deathparade.utils.Utils;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        float age = PreferenceManager.getDefaultSharedPreferences(this).getFloat(Utils.PREF_AGE, -1);
        Fragment fragment;
        if (age < 0) {
            fragment = new TestFragment();
        } else {
            fragment = new ResultFragment();
        }
        getFragmentManager().beginTransaction().add(R.id.fragment_container, fragment).commitAllowingStateLoss();

        MobileAds.initialize(getApplicationContext(), "ca-app-pub-6814342491962486~5612895057");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_about) {
            Fragment fragment = new AboutFragment();
            getFragmentManager().beginTransaction().addToBackStack(fragment.getClass().getSimpleName()).add(R.id.fragment_container, fragment).commitAllowingStateLoss();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
