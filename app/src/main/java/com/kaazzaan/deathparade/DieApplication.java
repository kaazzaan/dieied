package com.kaazzaan.deathparade;

import android.preference.PreferenceManager;
import android.support.multidex.MultiDexApplication;

import com.kaazzaan.deathparade.models.Country;
import com.kaazzaan.deathparade.models.Question;
import com.kaazzaan.deathparade.utils.CountriesParser;
import com.kaazzaan.deathparade.utils.QuestionsParser;
import com.kaazzaan.deathparade.utils.Utils;
import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.YandexMetricaConfig;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by daniyarsafiullin on 10/03/2017.
 */

public class DieApplication extends MultiDexApplication {
    public static final ArrayList<Question> questions = new ArrayList<>();
    public static final ArrayList<Country> countries = new ArrayList<>();
    public static float calculatedLifeExpectancy = 0;
    public static boolean male = false;
    public static float multipleFactor = 1f;

    @Override
    public void onCreate() {
        super.onCreate();
        loadCountries();
        loadQuestions();
        calculatedLifeExpectancy = PreferenceManager.getDefaultSharedPreferences(this).getFloat(Utils.PREF_AGE, 0);

        YandexMetricaConfig.Builder configBuilder = YandexMetricaConfig.newConfigBuilder("cb8b1382-614c-48e1-b36c-ad71c373eadf");
        //Реализуйте логику определения того, является ли запуск приложения первым. В качестве критерия вы можете использовать проверку наличия каких-то файлов (настроек, баз данных и др.), которые приложение создает в свой первый запуск.
        boolean isFirstApplicationLaunch = PreferenceManager.getDefaultSharedPreferences(this).getBoolean("ya_first_launch", true);
        if (isFirstApplicationLaunch) {
            //Передайте значение true, если не хотите, чтобы данный пользователь засчитывался как новый
            configBuilder.handleFirstActivationAsUpdate(true);
            PreferenceManager.getDefaultSharedPreferences(this).edit().putBoolean("ya_first_launch", false).apply();

        }
        if (BuildConfig.DEBUG) {
            configBuilder.setLogEnabled();
        }
        //Создание объекта расширенной конфигурации
        YandexMetricaConfig extendedConfig = configBuilder.build();
        // Инициализация AppMetrica SDK
        YandexMetrica.activate(getApplicationContext(), extendedConfig);
        YandexMetrica.enableActivityAutoTracking(this);
    }

    private void loadCountries() {
        CountriesParser parser = new CountriesParser(this);
        List<Country> tmp = parser.parse();
        countries.clear();
        countries.addAll(tmp);
    }

    private void loadQuestions() {
        QuestionsParser parser = new QuestionsParser(this);
        List<Question> tmp = parser.parse();
        questions.clear();
        questions.addAll(tmp);
    }
}
